#!/usr/bin/env fish

echo "choose a hostname (name for the computer, other computers may see it on networks)"
ask horse-arch-(cat /dev/urandom | base32 -w 8 | head -n1) >/etc/hostname

# TODO timezone
# look at how alpine install scripts for timezones and other parts
pushd /usr/share/zoneinfo
echo "i haven't figured out a good ui to set the timezone yet"
echo "so symlink it yourself"
echo "something like `ln -sf /usr/share/zoneinfo/*/* /etc/localtime`"
echo "use the `exit` command when done"
fish
popd

hwclock --systohc
sed -i "s/^#en_US.UTF-8/en_US.UTF-8/" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >/etc/locale.conf

set -l services
set -l user_services
begin
	set -l packages

	set -a packages kernel-modules-hook
	set -a services linux-modules-cleanup
	set -a packages git # needed for paru
	# base-devel
	set -a packages (pacman -Si base-devel | grep "Depends On" | sed "s/^.*:\\s*//" | string split "  ")
	set -e packages[(contains -i sudo $packages)]
	set -a packages opendoas
	# filesystem
	set -a packages ntfs-3g dosfstools btrfs-progs exfatprogs
	set -a packages gvfs-mtp gvfs-smb gvfs-gphoto2
	set -a packages gparted
	# networking
	set -a packages networkmanager dnsmasq
	set -a services NetworkManager
	set -a packages firewalld # this is literally the only program with network zones, it sucks
	set -a services firewalld
	# man
	set -a packages man-db man-pages texinfo tealdeer
	# boot
	set -a packages grub
	# fonts
	set -a packages noto-fonts-extra noto-fonts-cjk ttf-croscore ttf-carlito ttf-dejavu cantarell-fonts ttf-roboto otf-atkinson-hyperlegible ttf-fira-sans ttf-fira-mono wqy-zenhei

	# misc system tools
	set -a packages chrony
	set -a services chronyd
	set -a packages fwupd
	set -a packages fastfetch fzf ripgrep-all zoxide
	set -a packages p7zip # everything else is already installed or unneeded
	# audio
	set -a packages pipewire-audio pipewire-alsa pipewire-jack pipewire-pulse lib32-pipewire lib32-pipewire-jack wireplumber
	set -a packages rtkit realtime-privileges
	set -a packages fluidsynth soundfont-fluid
	set -a user_services fluidsynth
	# bluetooth
	set -a packages bluez bluez-utils blueman
	set -a services bluetooth

	# wayland
	set -a packages xorg-xwayland xorg-xhost xorg-xeyes
	set -a packages qt5-wayland qt6-wayland

	# display manager
	if true
		set -a packages weston sddm
		set -a services sddm
	end

	# desktop
	# TODO
	if true
		# sway
		#set -a packages acpilight
		#set -a packages lxqt-policykit
		set -a packages pcmanfm-qt lxqt-archiver foot
		#set -a packages qt5ct qt6ct
		set -a packages xsettingsd # also used on kde

		# kde
		if true
			set -a packages plasma-desktop
			set -a packages sddm-kcm
		end
	end

	# theme
	# TODO
	if true
		set -a packages breeze-icons
	end

	# thermal
	if true
		set -a packages tlp
		set -a services tlp
		if lscpu | grep -q GenuineIntel
			set -a packages thermald
			set -a services thermald
		end
	end

	# basic user programs
	set -a packages libreoffice-fresh krita inkscape blender keepassxc mpv qbittorrent ffmpeg yt-dlp krdc


	# development
	# TODO
	if false
		set -a packages mosh rsync sshfs
		set -a packages pnpm
		set -a packages rustup mold
		set -a packages miniupnpc
	end


	# platform specific stuff

	# bios/uefi
	if test -n (cat /sys/firmware/efi/fw_platform_size)
		set -a packages efibootmgr
	end

	#set -a packages os-prober


	# ucode
	if lscpu | grep -q GenuineIntel
		set -a packages intel-ucode
	end
	if lscpu | grep -q AuthenticAMD
		set -a packages amd-ucode
	end

	# gpu
	# TODO
	set -a packages mesa lib32-mesa
	if false
		set -a packages vulkan-radeon lib32-vulkan-radeon
	end
	if false
		set -a packages vulkan-intel lib32-vulkan-intel
	end
	if false # nvidia proprietary (open is part of mesa)
		set -a packages nvidia-dkms nvidia-utils lib32-nvidia-utils
		# TODO edit the mkinitcpio file
	end


	# fingerprint reader
	# TODO


	pacman -Syu --needed --noconfirm $packages
	pacman -Fy --noconfirm
end

#ln -sf /bin/doas /bin/sudo
#ln -sf /bin/doasedit /bin/sudoedit

# grub
switch (cat /sys/firmware/efi/fw_platform_size)
	case 64
		grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=horse-arch
	case 32
		grub-install --target=i386-efi --efi-directory=/boot --bootloader-id=horse-arch
	case ""
		echo "select a block device to install grub to:"
		lsblk -p -o +label
		set -l device (asklist "" (lsblk -no path))
		grub-install --target=i386-pc $device
end

# TODO nvidia kernel parameter (unless nvk is good enough)
sed -i "
	s/\\\$RESUME\\\$/"(
		# TODO hibernate?
		#if test (count $swapdevice) -gt 0
		#	echo " resume=$swapdevice[1]" # TODO pick hibernate device
		#end
	)"/
" /etc/default/grub

# disable hibernate
systemctl mask hibernate.target hybrid-sleep.target

grub-mkconfig -o /boot/grub/grub.cfg


passwd -l root
echo "select a username"
echo "name must be lowercase, and can include letters, digits, and dashes (cool-guy-360)"
echo "the name has to start with a letter"
echo "you'll be able to select a nickname next that can have any text"
set -l user
while true
	set user (ask "user")
	if echo $user | grep -q "^[a-z][-a-z0-9]*\$"
		break
	else
		echo "invalid username"
	end
end
echo "select a nickname"
echo "this can be any text and is usually used to display the name of the user in programs (🆒 GUY)"
set -l nickname (ask $user)
useradd -m -G games,realtime,wheel,video -c $nickname $user

passwd $user # TODO this only takes in one character for some reason? fix it

if not pacman -Q paru &> /dev/null
   set -l paru (runuser $user --session-command "mktemp -d")
	pushd $paru
	runuser $user --session-command "git clone 'https://aur.archlinux.org/paru-bin.git' ."
	runuser $user --session-command "makepkg -sir --noconfirm"
	popd
	rm -rf $paru
end

begin
	set -l packages

	set -a packages topgrade-bin dashbinsh doasedit-alternative librewolf-bin ttf-twemoji
	# theme
	# TODO
	if true
		set -a packages themix-gui-git themix-theme-oomox-git
	end
	# development
	# TODO
	if true
		set -a packages ventoy-bin
	end

	runuser $user --session-command "paru -Syu --noconfirm $(string join ' ' $packages)"
end

runuser $user --session-command "tldr -u"


# services
systemctl enable $services fstrim.timer
runuser $user --session-command "systemctl --user enable $user_services"
# TODO check that user pipewire is already enabled
# TODO configure fluidsynth so that it actually works

# TODO configure fluidsynth
