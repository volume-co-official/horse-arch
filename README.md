# 🐎 arch

## installing

note that this whole thing is still a hacky mess that could break at any time,
use at your own risk

1. get and boot an endeavouros live cd (archiso would probably work if you fiddle with it but endeavouros is easier)
2. create and mount your partitions at /mnt, and swapon your swap devices
	you need at least a root partition and if using uefi, an efi partition at /mnt/boot
	i reccomend a 512mb efi partition, a btrfs root partition, and a 16gb-24gb swap partition
3. run these commands
```
sudo su
timedatectl set-ntp true
pacman -Sy archlinux-keyring fish
curl "https://git.disroot.org/ficial/horse-arch/archive/main.tar.gz" --output horse-arch.tar.gz
gzip -d horse-arch.tar.gz
tar xf horse-arch.tar
cd horse-arch
fish main.fish
```
4. follow instructions in script. showing a shell prompt does not mean it is finished! read some of the output

## developing

everything in overlay.tar is placed onto the system before using `pacstrap`,
pretty much just to set up config files and such.
note that overlay keeps all the same permissions, so everything needs to be owned by root.

i haven't seen any gui archivers that can manage permissions nicely, so just use the commandline and `fakeroot` to manage the files.
i suggest installing tealdeer and using `tldr tar`.
