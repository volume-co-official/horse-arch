#!/usr/bin/env fish

source ./common.fish

if not pacman -Syi base
	echo "pacman not working, are you connected to the internet?"
	exit 1
end

echo "---"
echo "MOUNT"
echo "---"
mount | grep "on /mnt" | cat
echo "---"
echo "SWAP"
echo "---"
swapon
echo "make sure to create, format, and mount/swap partitions first!"
echo "all the relevant partitions are expected to be empty and mounted appropriately in /mnt"
echo "make sure to have a root partition, and if you're using UEFI, an efi partition on /mnt/boot"
echo "you'll likely also want a swap partition between 1x and 2x your system ram"

switch (cat /sys/firmware/efi/fw_platform_size)
	case 64
		echo "grub target will be 64 bit UEFI, check to make sure this is correct"
	case 32
		echo "grub target will be 32 bit UEFI, check to make sure this is correct"
	case ""
		echo "grub target will be BIOS (not uefi), check to make sure this is correct"
	case "*"
		echo "fw_platform_size is some weird unknown value!"
		echo "normally it would be 64, 32, or nothing to indicate uefi/bios"
		exit 1
end
echo "# say yes to continue ONLY if you have checked all of these things and they are correct"
if not askyesno no
	exit 1
end

# empty bash files from skel
rm -rf /mnt/etc/skel/.bash*
# extract overlay into system
tar xf overlay.tar --directory=/mnt

# make /mnt/etc/fstab
echo -n > /mnt/etc/fstab
set -lx swapdevice
begin
	function getdevice
		set -l uuid (lsblk $argv[1] -rno uuid)
		if test -n $uuid
			echo UUID=$uuid
		else
			echo $argv[1]
		end
	end
	for mountline in (mount | grep "on /mnt")
		set -l words (string split " " $mountline)
		set -l device (getdevice $words[1])
		set -l mountpoint (echo $words[3] | sed "s/^\/mnt\/*/\//")
		set -l filesystem $words[5]
		set -l options defaults
		set -l check 0
		if test $filesystem = btrfs
			set -a options subvol=(btrfs subvolume show $mountpoint | head -n1)
		else
			switch $mountpoint
				case "/"
					set check 1
				case "/*"
					set check 2
			end
		end
		echo "$device $mountpoint $filesystem $(string join , $options) 0 $check" >> /mnt/etc/fstab
	end
	set -l swaplines (swapon --raw --noheadings --show=NAME,PRIO)
	if test (count $swaplines) -gt 1
		echo "pick a swap device to use for hibernate (number on the left)"
		echo "0 for no hibernate"
		set -l fullswaplist swapon --show=NAME,LABEL,TYPE,SIZE,PRIO,UUID
		set -l swaplist $fullswaplist[2..]
		echo "   $fullswaplist[1]"
		for i in (seq (count $swaplist))
			echo "$i  $swaplist[i]"
		end
		set swapdevice (asklist 1 (seq (count $swaplist)))
	end
	for swapline in $swaplines
		set -l words (string split " " $swapline)
		set -l device (getdevice $words[1])
		set -l options defaults
		if test $words[2] -ge 0
			set -a options pri=$words[2]
		end
		echo "$device none swap $(string join , $options) 0 0" >> /mnt/etc/fstab
	end
end

echo "```"
cat /mnt/etc/fstab
echo "```"
echo "the default shell is now open so you can work on the fstab file if there are any errors"
echo "use the `exit` command to continue"
fish

# pacstrap
pacstrap -K /mnt --needed base linux-zen linux-zen-headers linux-firmware fish

begin
	set -l script (mktemp -p /mnt/root)
	cat common.fish in-chroot.fish > $script
	arch-chroot /mnt /bin/fish (echo $script | sed "s/\\/mnt//")
	rm $script
	# TODO check that chroot script ran correctly
end

echo "finished! you can reboot and start using your system now"
