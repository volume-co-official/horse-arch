#!/usr/bin/env fish

# reads input, $argv[1] is default result
function ask
	if test -z $argv[1]
		read -p "echo '> '"
	else
		read -p "echo '[$argv[1]]> '" input
		if test -z $input
			echo $argv[1]
		else
			echo $input
		end
	end
end

# 2.. is valid responses
function asklist
	while true
		set -l answer (ask $argv[1])
		switch $answer
			case $argv[2..]
				echo $answer
				return 0
			case "*"
				echo "invalid response"
				# this loops in the `while true`
		end
	end
end


function askyesno
	while true
		switch (ask $argv[1])
			case y yes
				return 0
			case n no
				return 1
			case "*"
				echo "should be yes or no!"
				# this loops in the `while true`
		end
	end
end

